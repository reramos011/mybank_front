/**
 * @customElement
 * @summary Custom element with heading semantics that allows to configure the heading level.
 * @polymer
 * @demo demo/index.html
 * @extends {Polymer.Element}
 */
class cellsHeading extends Polymer.Element {
  static get is() {
    return 'cells-heading';
  }

  static get properties() {
    return {
      /**
       * Allows to define an aria-level for the main header text.
       * If 0 is provided, the header text won't be treated as a heading.
       */
      level: {
        type: Number,
        value: 2,
        notify: true,
        observer: '_levelChanged'
      }
    };
  }

  connectedCallback() {
    super.connectedCallback();
    this._ensureAttribute('aria-level', 2);
  }

  _levelChanged(level) {
    if (level >= 1 && level <= 6) {
      this.setAttribute('aria-level', level);
      this.setAttribute('role', 'heading');
    } else {
      this.removeAttribute('aria-level');
      this.removeAttribute('role');
    }
  }
}

customElements.define(cellsHeading.is, cellsHeading);
