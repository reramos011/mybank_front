window.CellsBehaviors = window.CellsBehaviors || {};

/**
 * ![cells-input-validations-behavior screenshot](cells-input-validations-behavior.png)
 *
 * # cells-input-validations-behavior
 *
 * ![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg) ![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)
 *
 * [Demo of component in Cells Catalog](https://catalogs.platform.bbva.com/cells)
 *
 * cells-input-validations-behavior is a behaviour to format and validate custom user logins. This component receives a configuration object with validation and one mask parameters.
 *
 * Example config object to *validate* input value:
 * ```js
 *   {
 *     autoValidate: false,
 *     inputStatusValidate: true,
 *     inputType: 'text',
 *     allowedValue: "rut",
 *     errorMessage: 'rutMsg',
 *     errorMessageIcon: 'coronita:error',
 *     mask: 'rut',
 *     maxLength: '13'
 *   }
 * ```
 * Example config object to validate and *apply masked format (maskEnabled)* input value:
 *
 * ```js
 *   {
 *     autoValidate: false,
 *     inputStatusValidate: true,
 *     inputType: 'text',
 *     allowedValue: "rut",
 *     errorMessage: 'rutMsg',
 *     errorMessageIcon: 'coronita:error',
 *     mask: 'rut',
 *     maxLength: '13',
 *     maskEnabled: true
 *   }
 * ```
 *
 * In possible validate more than one regular expression setting on array to property allowedValue or allowedPassword value
 *
 * ```js
 *     {
 *     autoValidate: true,
 *     inputType: 'password',
 *     allowedPasswordValue: ['mxRepeatPassword', 'mxFollowedPassword'],
 *     errorPasswordMessage: ['mxRepeatPassword', 'mxFollowedPassword'],
 *   }
 * ```
 *
 * @polymer
 * @customElement
 * @summary Behavior to mask and or validate input value from app configuration pages
 * @extends {Polymer.Element}
 * @polymerBehavior Cells.InputValidationsBehavior
 * @demo demo/index.html
 * @hero cells-input-validations-behavior.png
 */
window.CellsBehaviors.InputValidationsBehavior = {
  properties: {
    /**
     * Regex pattern for each validation type.
     */
    regexps: {
      type: Object,
      value: {
        rut: '\\d{3}.\\d{3}.\\d{3}-k|\\d{3}.\\d{3}.\\d{3}-\\d{1}|\\d{9}?k|\\d{10}',
        mxPhone: '\\d{2}\\s\\d{8}|\\d{10}',
        ciUruguay: '^\\d{7}-\\d{1}?$',
        mxRepeatPassword: '^(?!.*(.)\\1{3})',
        mxFollowedPassword: '^(?!.*(0123|1234|2345|3456|4567|5678|6789))',
        uyMinPassword: '^[\\d\\w]{8,}$',
        uyMinSpaPassword: '^[\\d\\wñÑ]{8,}$',
        emailCiUser: '^[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$|^\\d{8}$', // eslint-disable-line no-useless-escape
        email: '^[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$', // eslint-disable-line no-useless-escape
        ci: '^\\d{8}$'
      }
    },
    /**
     * Translation keys for each validation type.
     */
    messages: {
      type: Object,
      value: {
        rutMsg: 'cells-rut-error-message',
        mxPhoneMsg: 'cells-mx-phone-error-message',
        ciUruguayMsg: 'cells-ciUruguay-error-message',
        mxRepeatPassword: 'cells-mx-password-repeat-error-message',
        mxFollowedPassword: 'cells-mx-password-followed-error-message',
        uyMinPassword: 'cells-change-password-enough-characters-error-message',
        emailMsg: 'cells-email-error-message',
        ciLengthMsg: 'cells-ci-length-error-message'
      }
    },
    /**
     * Mask formats for each validation type.
     */
    masks: {
      type: Object,
      value: {
        rut: ['', '', '', '.', '', '', '', '.', '', '', '', '-', ''],
        mxPhone: ['', '', ' ', '', '', '', '', '', '', '', ''],
        peAccountNumber: ['', '', '', '', '-', '', '', '', '', '-', '', '', '', '', '', '', '', '', '', ''],
        ciUruguay: ['', '', '', '', '', '', '', '-', ''],
        mxPassword: ['', '', '', '', '', '']
      }
    }
  },

  _getPattern: function(value) {
    return this.regexps[ value ] || value;
  },

  _getMultiplePattern: function(value) {
    if (!Array.isArray(value)) {
      value = [ value ];
    }
    return value && value.length && value.reduce(function(acc, item) {
      acc.push(this.regexps[ item ]);
      return acc;
    }.bind(this), []);
  },

  _getErrorMessages: function(value) {
    return this.t(this.messages[ value ] || value) || '';
  },
  _getMultipleErrorMessages: function(value) {
    return this.t(this.messages[ value ] || value) || '';
  },

  _getMasked: function(value, type, maskEnabled) {
    if (value && type && maskEnabled) {
      value = value.replace(/[^a-zA-Z 0-9.]|\s/g, '');
      var pattern = this.masks[ type ];
      var aux = value.split('').slice(0, pattern.length);
      var mask = [];
      var acc = 0;
      aux.some(function(item) {
        if (!pattern[ acc ]) {
          mask.push(item);
        } else {
          if (pattern[ acc ] !== item) {
            // copy
            mask.push(pattern[ acc ], item);
            acc++;
          } else {
            // write
            mask.push(item);
          }
        }
        acc++;
      });
      return mask.join('').toString();
    }

    return value;
  },

  _getUnMasked: function(value, type) {
    if (value && type) {
      var aux = value.split('');
      var pattern = this.masks[ type ];
      var input = '';
      var acc = 0;
      aux.some(function(item) {
        if (!pattern[ acc ] || pattern[ acc ] !== item) {
          input += item;
        }
        acc++;
      });
      return input;
    }
  }
};