/**
 * ![cells-credentials-form screenshot](cells-credentials-form.png)
 *
 * # cells-credentials-form
 *
 * ![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg) ![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)
 *
 * [Demo of component in Cells Catalog](https://catalogs.platform.bbva.com/cells)
 *
 * This component provides a form with basic user-password authentication.
 *
 * It uses `cells-molecule-input` to present an ID field and a password field. ID field can receive a clear field icon; password field can receive both a clear field and a toggle password icon.
 *
 * ```html
 * <cells-credentials-form clear-id-icon="coronita:close" clear-pwd-icon="coronita:close" toggle-pwd-icon="coronita:visualize" toggle-pwd-icon-toggled="coronita:hide"></cells-credentials-form>
 * ```
 *
 * The form will show a "Did you forget your password?" button. You can show also a "Are you not an user? Register" button adding the `register` attribute.
 *
 * ```html
 * <cells-credentials-form register clear-id-icon="coronita:close" clear-pwd-icon="coronita:close" toggle-pwd-icon="coronita:visualize" toggle-pwd-icon-toggled="coronita:hide"></cells-credentials-form>
 * ```
 *
 * Also, using the `autofocus` attribute, the user Id field will receive focus when the page is loaded.
 *
 * ```html
 * <cells-credentials-form autofocus clear-id-icon="coronita:close" clear-pwd-icon="coronita:close" toggle-pwd-icon="coronita:visualize" toggle-pwd-icon-toggled="coronita:hide"></cells-credentials-form>
 * ```
 *
 * Tapping any of the buttons will fire an event. Activating the submit button will fire an event passing the user Id and user Password field values as payload. The submit button will be disabled until both fields are filled.
 *
 * The component uses the attribute `user-name`, with the user name as value, to treat the user as already logged. This will hide the userId field and show a user greeting and a "change user" button above the password field.
 *
 * ```html
 * <cells-credentials-form user-name="John" user-id="loremipsum" clear-pwd-icon="coronita:close" toggle-pwd-icon="coronita:visualize" toggle-pwd-icon-toggled="coronita:hide"></cells-credentials-form>
 * ```
 *
 * The component can optional use cells-input-validation-behavior to validate and mask input user login value.
 *
 * ```html
 * <cells-credentials-form user-name="John" user-id="loremipsum"  input-user-options="[[inputUserOptions]]"></cells-credentials-form>
 * ```
 * ```js
 *   {
 *     autoValidate: true,
 *     inputStatusValidate: true,
 *     inputType: 'text',
 *     allowedValue: "rut",
 *     errorMessage: 'rutMsg',
 *     errorMessageIcon: 'coronita:error',
 *     mask: 'rut',
 *     maxLength: '13'
 *   }
 *
 * ```
 *
 * Also, the component can show a selector for select a document type and validate the identity of the user. The selector is shown when document-type property is filled.
 *
 * ````html
 *  <cells-credentials-form id="ui"
 *     document-type="{{itemSelected}}"
 *     clear-id-icon="coronita:close"
 *     clear-pwd-icon="coronita:close"
 *     toggle-pwd-icon="coronita:visualize"
 *     toggle-pwd-icon-toggled="coronita:hide">
 *   </cells-credentials-form>
 * ````
 *
 * Document type is an object that has the rules to validate username, error message and other stuff related with the userId validation.
 *
 * ````json
 * {
 *     id: 'M',
 *     allowedValue: '^[\\w-_/.]{3,15}$',
 *     maxLength: 15,
 *     max: 15,
 *     min: 3,
 *     type: 'text',
 *     errorMessageIcon: 'coronita:alert',
 *     errorMessage: 'glomo-login-generic-error-message',
 *     name: 'glomo-login-document-type-M'
 *   }
 * ````
 *
 * The selector shown is a fake selector that fires an "select-document-type" event when is clicked. Usually the selector values are represented in into a cells-modal-selector as it can be seen in the demo.
 *
 *
 * ## Icons
 *
 * Since this component uses icons, it will need an [iconset](https://bbva.cellsjs.com/guides/best-practices/cells-icons.html) in your project as an [application level dependency](https://bbva.cellsjs.com/guides/advanced-guides/application-level-dependencies.html). In fact, this component uses an iconset in its demo.
 *
 * ## Styling
 *
 * The following custom properties and mixins are available for styling:
 *
 * ### Custom Properties
 * | Custom Property                                                        | Selector                                               | CSS Property                            | Value        |
 * | ---------------------------------------------------------------------- | ------------------------------------------------------ | --------------------------------------- | ------------ |
 * | --cells-fontDefault                                                    | :host                                                  | font-family                             | sans-serif   |
 * | --cells-credentials-form-input-min-height                              | :host                                                  | --cells-molecule-input-field-min-height | 3.75rem      |
 * | --cells-credentials-form-fake-selector-bg-color                        | .credentials-form .fake-selector                       | background-color                        | --bbva-100   |
 * | --cells-credentials-form-fake-selector-modal-icon-color                | .credentials-form .fake-selector .modal-icon           | color                                   | --bbva-white |
 * | --cells-credentials-form-fake-selector-label-color                     | .credentials-form .fake-selector .label                | color                                   | --bbva-500   |
 * | --cells-credentials-form-fake-selector-label-selected-color            | .credentials-form .fake-selector .label.selected       | color                                   | --bbva-400   |
 * | --cells-credentials-form-fake-selector-placeholder-color               | .credentials-form .fake-selector .placeholder          | color                                   | --bbva-600   |
 * | --cells-credentials-form-fake-selector-placeholder-selected-icon-color | .credentials-form .fake-selector .placeholder.selected | color                                   | --bbva-white |
 * ### @apply
 * | Mixins                                                                  | Selector                                                                                     | Value |
 * | ----------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- | ----- |
 * | --cells-credentials-form-input-field-withcontent                        | :host > --cells-molecule-input-withcontent-field-input:                                      | {}    |
 * | --cells-credentials-form-input-wrapper                                  | :host > --cells-molecule-input-wrapper:                                                      | {}    |
 * | --cells-credentials-form                                                | :host                                                                                        | {}    |
 * | --cells-molecule-input-has-content-wrapper                              | .credentials-form > --cells-credentials-form-cells-molecule-input-has-content-wrapper:       | {}    |
 * | --cells-credentials-form-form                                           | .credentials-form                                                                            | {}    |
 * | --cells-credentials-form-inputs                                         | .credentials-form__inputs                                                                    | {}    |
 * | --cells-credentials-form-inputs-input                                   | .credentials-form__inputs cells-molecule-input                                               | {}    |
 * | --cells-credentials-form-inputs-input-withcontent                       | .credentials-form__inputs cells-molecule-input[has-content]                                  | {}    |
 * | --cells-credentials-form-submit                                         | .credentials-form__submit                                                                    | {}    |
 * | --cells-credentials-form-submit-button                                  | .credentials-form__submit button[type="submit"]                                              | {}    |
 * | --cells-credentials-form-extra-actions                                  | .credentials-form__extra-actions                                                             | {}    |
 * | --cells-credentials-form-extra-actions-logged                           | :host([is-logged]) .credentials-form__extra-actions                                          | {}    |
 * | --cells-credentials-form-actions                                        | .credentials-form__forgotten-pwd                                                             | {}    |
 * | --cells-credentials-form-actions                                        | .credentials-form__change-user                                                               | {}    |
 * | --cells-credentials-form-actions                                        | .credentials-form__register                                                                  | {}    |
 * | --cells-credentials-form-actions-buttons                                | .credentials-form__forgotten-pwd button                                                      | {}    |
 * | --cells-credentials-form-actions-buttons                                | .credentials-form__change-user button                                                        | {}    |
 * | --cells-credentials-form-actions-buttons                                | .credentials-form__register button                                                           | {}    |
 * | --cells-credentials-form-forgotten-pwd                                  | .credentials-form__forgotten-pwd                                                             | {}    |
 * | --cells-credentials-form-change-user                                    | .credentials-form__change-user                                                               | {}    |
 * | --cells-credentials-form-register                                       | .credentials-form__register                                                                  | {}    |
 * | --cells-credentials-form-register-is-logged                             | :host([is-logged]) .credentials-form__register                                               | {}    |
 * | --cells-credentials-form-user                                           | .credentials-form__user                                                                      | {}    |
 * | --cells-credentials-form-user-image                                     | .credentials-form__user__image                                                               | {}    |
 * | --cells-credentials-form-user-greeting                                  | .credentials-form__user__greeting                                                            | {}    |
 * | --cells-credentials-form-user-name                                      | .credentials-form__user__name                                                                | {}    |
 * | --cells-credentials-form-fake-selector                                  | .credentials-form .fake-selector                                                             | {}    |
 * | --cells-credentials-form-fake-selector-focus                            | .credentials-form .fake-selector:focus                                                       | {}    |
 * | --cells-credentials-form-fake-selector-modal-icon                       | .credentials-form .fake-selector .modal-icon                                                 | {}    |
 * | --layout-vertical                                                       | .credentials-form .fake-selector .header-texts                                               | {}    |
 * | --cells-credentials-form-fake-selector-label                            | .credentials-form .fake-selector .label                                                      | {}    |
 * | --cells-credentials-form-fake-selector-placeholder                      | .credentials-form .fake-selector .placeholder                                                | {}    |
 * | --cells-credentials-form-animated-user                                  | :host([animated]) .credentials-form__user                                                    | {}    |
 * | --cells-credentials-form-animated-change-user                           | :host([animated]) .credentials-form__change-user                                             | {}    |
 * | --cells-credentials-form-animated-inputs-button                         | :host([animated]) .credentials-form__inputs button                                           | {}    |
 * | --cells-credentials-form-animated-inputs-button-molecule-input-not-last | :host([animated]) .credentials-form__inputs button ~ cells-molecule-input:not(:last-of-type) | {}    |
 * | --cells-credentials-form-animated-cells-molecule-input-first            | :host([animated]) .credentials-form__inputs cells-molecule-input:first-of-type               | {}    |
 * | --cells-credentials-form-animated-cells-molecule-input-last             | :host([animated]) .credentials-form__inputs cells-molecule-input:last-of-type                | {}    |
 * | --cells-credentials-form-animated-submit-button                         | :host([animated]) .credentials-form__submit button                                           | {}    |
 * | --cells-credentials-form-animated-extra-actions-p                       | :host([animated]) .credentials-form__extra-actions p                                         | {}    |
 *
 * @polymer
 * @customElement
 * @summary Provides a form for user:password credentials authentication
 * @extends {Polymer.Element}
 * @polymerBehavior Cells.CellsCredentialsForm
 * @demo demo/index.html
 * @hero cells-credentials-form.png
 */
class CellsCredentialsForm extends Polymer.mixinBehaviors([CellsBehaviors.i18nBehavior, CellsBehaviors.InputValidationsBehavior], Polymer.Element) {
  static get is() {
    return 'cells-credentials-form';
  }
  /* global moment */
  static get properties() {
    return {
      /**
       * User first name
       */
      userName: {
        type: String,
        notify: true,
        value: ''
      },
      /**
       * User avatar
       */
      userAvatar: {
        type: String,
        value: ''
      },
      /**
       * Heading level to apply to user name and greeting
       */
      headingLevel: {
        type: Number,
        value: 1
      },
      /**
       * Invalid status of the form
       */
      invalid: {
        type: Boolean,
        reflectToAttribute: true,
        notify: true
      },
      /**
       * User identification
       */
      userId: {
        type: String,
        value: ''
      },
      /**
       * Temporal User identification
       */
      _userId: {
        type: String,
        notify: true,
        observer: '_onUserIdChanged'
      },
      /**
       * User password, access key or similar
       */
      userPassword: {
        type: String,
        notify: true,
        value: ''
      },
      /**
       * Document Type
       */
      documentType: {
        type: Object,
        observer: '_onDocumentTypeChanged'
      },
      /**
       * Size icons
       */
      iconsSize: {
        type: Number,
        value: 18
      },
      /**
       * Clear icon for user field
       */
      clearIdIcon: { type: String },
      /**
       * Clear icon for password field
       */
      clearPwdIcon: { type: String },
      /**
       * Toggle icon for password field
       */
      togglePwdIcon: { type: String },
      /**
       * Toggle icon for password field, toggled version
       */
      togglePwdIconToggled: { type: String },
      /**
       * Returns true if userId and userPassword are not null.
       */
      _isSubmitEnabled: {
        type: Boolean,
        computed: '_computeIsSubmitEnabled(userId, userPassword)'
      },
      /**
       * If true, autofocus in user ID field, if available
       */
      autofocus: {
        type: Boolean,
        value: false
      },
      /**
       * Shows a register button
       */
      register: {
        type: Boolean,
        value: false
      },
      /**
       * Indicates if user is logged or not
       */
      isLogged: {
        type: String,
        reflectToAttribute: true,
        computed: '_computeIsLogged(userName)'
      },
      /**
       * Translation key for user input
       */
      labelUserId: {
        type: String,
        value: 'NIF, NIE, Passport, email'
      },
      /**
       * Translation key for user password
       */
      labelPassword: {
        type: String,
        value: 'Access key'
      },
      /**
       * Translation key for user password
       */
      labelUserIdClearIcon: {
        type: String,
        value: 'Clear field'
      },
      /**
       * Translation key for user password
       */
      labelPasswordToggleIcon: {
        type: String,
        value: 'Show/hide password'
      },
      /**
       * Translation key for user password
       */
      labelPasswordClearIcon: {
        type: String,
        value: 'Clear field'
      },
      /**
       * Translation key for button
       */
      labelLogin: {
        type: String,
        value: 'Login'
      },
      /**
       * Translation key for 'forget your password?' button
       */
      labelForgetPassword: {
        type: String,
        value: 'Did you forget your access key?'
      },
      /**
       * Translation key for user not logged
       */
      labelNewUser: {
        type: String,
        value: 'Are you not an user?'
      },
      /**
       * Translation key for user registration
       */
      labelRegister: {
        type: String,
        value: 'Register'
      },
      /**
       * Translation key for user logout
       */
      labelLogout: {
        type: String,
        value: 'Change user'
      },
      /**
       * Translation key for greeting
       */
      labelGreeting: {
        type: String,
        value: 'Hi'
      },
      /**
       * Error password validation message
       */
      errorPasswordMessage: {
        type: String,
        value: ''
      },
      /**
       * Input password type
       */
      inputPasswordType: {
        type: String,
        value: 'password'
      },
      /**
       * input user login field validations
       */
      inputUserOptions: {
        type: Object,
        value: function() {
          return {
            autoValidate: false,
            autoValidatePassword: false,
            inputStatusValidate: false,
            autoMask: true,
            inputType: '',
            inputPasswordType: '',
            allowedValue: '',
            allowedPasswordValue: '',
            errorMessage: '',
            errorPasswordMessage: [],
            errorMessageIcon: '',
            mask: '',
            passwordMask: '',
            maxLength: '',
            customPattern: '',
            customPasswordPattern: ''
          };
        }
      },
      /**
       * Autocapitalize input
       */
      autocapitalize: {
        type: String,
        value: 'none'
      },
      /**
       * Motion to show of all the elements
       */
      animated: {
        type: Boolean,
        reflectToAttribute: true
      },
      /**
       * Allow complete value removal if backspace key is pressed and input type is password
       */
      backspacePwdFullDelete: {
        type: Boolean,
        value: false
      },
      /**
       * Show lite or full greetings heading
       */
      lite: {
        type: Boolean,
        value: false
      },
      /**
       * Show or hide info next to register button
       */
      showRegisterInfo: {
        type: Boolean,
        value: false
      },
      /**
       * Hour to set greeting morning
       */
      hourGreetingMorning: {
        type: Number,
        value: 5
      },
      /**
       * Hour to set greeteng noon
       */
      hourGreetingNoon: {
        type: Number,
        value: 13
      },
      /**
       * Hour to ser greeting nigth
       */
      hourGreetingNight: {
        type: Number,
        value: 21
      },
      /**
       * Internationalization key for greeting
       */
      greetingKey: { type: String }
    };
  }

  static get observers() {
    return [
      '_clearInvalid(userId, userPassword)',
      '_invalidObserver(invalid)'
    ];
  }

  ready() {
    super.ready();
    this.addEventListener('user-auto-mask', this.autoMaskStatus.bind(this));
    this.addEventListener('cells-molecule-input-validate', this._onInputValidate.bind(this));
    this.addEventListener('validation-error-type', this._setErrorMessage.bind(this));
  }

  connectedCallback() {
    super.connectedCallback();
    this._setAutofocus();
    this.setGreetingMessage();
  }
  /**
   * Resets user to initial state.
   */
  resetUser() {
    this.userId = '';
    this._userId = '';
    this.userName = '';
    this.userAvatar = '';
    if (this.shadowRoot.querySelector('#user-input-id')) {
      this.shadowRoot.querySelector('#user-input-id').validate();
    }
  }
  /**
   * Resets password to initial state.
   */
  resetPassword() {
    this.userPassword = '';
    if (this.shadowRoot.querySelector('#user-input-pwd')) {
      this.shadowRoot.querySelector('#user-input-pwd').validate();
    }
  }
  /**
   * Resets component to initial state.
   */
  reset() {
    this.resetUser();
    this.resetPassword();
  }

  _onKeyPress(e) {
    if (e.keyCode === 13) {
      e.preventDefault();
      this.$.loginBtn.click();
    }
  }

  _computeIsLogged(userName) {
    return !!userName;
  }
  /**
   * Returns boolean for isLogged based on userName property
   * @param {String} userName
   * @return {Boolean}
   */
  _setAutofocus() {
    Polymer.RenderStatus.afterNextRender(this, function() {
      var input;
      if (this.autofocus && (input = this.shadowRoot.querySelector('#user-input-id:not([hidden])'))) {
        this.async(input.focus.bind(input), 500);
      }
    });
  }
  /*
     * Chage (enable and disable ) automask user input data
     */
  autoMaskStatus(autoMask) {
    if (autoMask && this.inputUserOptions) {
      var mask = !autoMask.detail ? this.inputUserOptions.maskEnabled = null : this.inputUserOptions.maskEnabled = this.inputUserOptions.mask;
      return mask;
    }
  }
  /**
   * Toggles invalid state on input fields
   * @param {String} newvalue
   */
  _invalidObserver(newvalue) {
    this.shadowRoot.querySelector('#user-input-id').invalid = newvalue;
    this.shadowRoot.querySelector('#user-input-pwd').invalid = newvalue;
  }
  /**
   * Change form invalid property when an input changes
   */
  _clearInvalid() {
    if (this.invalid) {
      this.invalid = false;
    }
  }
  /**
   * Returns true if user, password and documentType (only if it is required)
   * @param {String} userId
   * @param {String} userPwd
   * @return {Boolean}
   */
  _computeIsSubmitEnabled(userId, userPwd) {
    return !!(this.documentType ? userId && userPwd && this.documentType.id : userId && userPwd);
  }
  /**
   * Action done when submitting the form.
   *
   * @param {Event} e
   */
  _doCredentialsUserPwd(e) {
    e.preventDefault();
    if (this._checkIfSubmit()) {
      this.dispatchEvent(new CustomEvent('login', {
        bubbles: true,
        composed: true,
        detail: {
          userId: this.userId,
          password: this.userPassword,
          username: this.userName,
          documentType: this.documentType
        }
      }));
    }
  }
  /**
   * Fired when submit button is activated, gets userId and userPassword as payload
   * @event login
   */
  /**
   * Checks if user/password fields are invalid before form submission
   *
   */
  _checkIfSubmit() {
    if (this.invalid !== undefined && this.invalid) {
      return false;
    } else {
      var userInvalid = false;
      if (!this.isLogged) {
        this.shadowRoot.querySelector('#user-input-id').validate();
        userInvalid = this.shadowRoot.querySelector('#user-input-id').invalid;
      }
      this.shadowRoot.querySelector('#user-input-pwd').validate();
      if (userInvalid || this.shadowRoot.querySelector('#user-input-pwd').invalid) {
        return false;
      }
    }
    return true;
  }
  /**
   * Fires event when register button is activated
   *
   * @param {Event} e
   */
  _doRegister(e) {
    e.preventDefault();
    this.dispatchEvent(new CustomEvent('request-register', {
      bubbles: true,
      composed: true
    }));
    /**
     * Fired when register button is activated
     * @event request-register
     */
  }
  /**
   * Fires event when forget password button is activated
   *
   * @param {Event} e
   */
  _onForgetPwd(e) {
    e.preventDefault();
    this.dispatchEvent(new CustomEvent('request-password-recover', {
      bubbles: true,
      composed: true
    }));
    /**
     * Fired when forget password button is activated
     * @event request-password-recover
     */
  }
  /**
   * Fires event when change user button is activated
   *
   * @param {Event} e
   */
  _changeUser(e) {
    e.preventDefault();
    this.dispatchEvent(new CustomEvent('request-change-user', {
      bubbles: true,
      composed: true
    }));
    /**
     * Fired when change user button is activated
     * @event request-change-user
     */
  }
  /**
   * fires event when user taps in document type select
   *
   * @param {Event} e
   */
  _onUserIdChanged(_userId) {
    var type = (this.inputUserOptions || {}).mask;
    this.userId = this._getUnMasked(_userId, type) || _userId;
  }
  /**
   * Sets username input validation according to the document type selected and set cursor focus
   *
   * @param {String} documentType
   */
  _openDocumentTypeSelector(e) {
    e.preventDefault();
    this.dispatchEvent(new CustomEvent('select-document-type', {
      bubbles: true,
      composed: true
    }));
    /**
     * Fired when user taps in document type selector
     * @event on-tap-document-type
     */
  }

  _onInputValidate(ev, validation) {
    if (!validation.valid) {
      var message;
      if (validation.value.length < this.inputUserOptions.min) {
        message = this.t('cells-credentials-form-min-characters-error-message');
      } else {
        message = this._getErrorMessages(this.inputUserOptions.errorMessage);
      }
      this.shadowRoot.querySelector('#' + ev.detail.id).set('errorMessage', message);
    }
  }

  _onDocumentTypeChanged(documentType, previousDocumentType) {
    if (documentType) {
      this.reset();
      this.set('inputUserOptions', {
        allowedValue: documentType.allowedValue,
        maxLength: documentType.maxLength || '',
        inputStatusValidate: documentType.inputStatusValidate || true,
        errorMessageIcon: documentType.errorMessageIcon,
        errorMessage: documentType.errorMessage,
        inputType: documentType.inputType,
        max: documentType.max,
        min: documentType.min
      });
      if (previousDocumentType && documentType.id !== previousDocumentType.id) {
        this.autofocus = true;
        this._setAutofocus();
      }
    }
  }
  /**
   * Set validation error message
   * @param {String} type
   */
  _setErrorMessage(type) {
    this.errorPasswordMessage = ((this.inputUserOptions || {}).errorPasswordMessage || {})[ type.detail ] || '';
  }
  /**
   * Focus on specific input
   * @param {String} input
   */
  focusInput(input) {
    this.shadowRoot.querySelector('#user-input-' + input).focus();
  }
  /**
   * Set greeting message according to current time
   */
  setGreetingMessage() {
    var currentTime = moment(new Date());
    var morning = currentTime.clone().hour(this.hourGreetingMorning).minutes(0).seconds(0);
    var noon = currentTime.clone().hour(this.hourGreetingNoon).minutes(0).seconds(0);
    var night = currentTime.clone().hour(this.hourGreetingNight).minutes(0).seconds(0);
    if (currentTime.isBetween(morning, noon, null, '[)')) {
      this.greetingKey = 'cells-greeting-morning';
    } else if (currentTime.isBetween(noon, night, null, '[)')) {
      this.greetingKey = 'cells-greeting-evening';
    } else {
      this.greetingKey = 'cells-greeting-night';
    }
  }
}

window.customElements.define(CellsCredentialsForm.is, CellsCredentialsForm);
