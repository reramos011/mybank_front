![cells-credentials-form screenshot](cells-credentials-form.png)

# cells-credentials-form

![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg) ![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)

[Demo of component in Cells Catalog](https://catalogs.platform.bbva.com/cells)

This component provides a form with basic user-password authentication.

It uses `cells-molecule-input` to present an ID field and a password field. ID field can receive a clear field icon; password field can receive both a clear field and a toggle password icon.

```html
<cells-credentials-form clear-id-icon="coronita:close" clear-pwd-icon="coronita:close" toggle-pwd-icon="coronita:visualize" toggle-pwd-icon-toggled="coronita:hide"></cells-credentials-form>
```

The form will show a "Did you forget your password?" button. You can show also a "Are you not an user? Register" button adding the `register` attribute.

```html
<cells-credentials-form register clear-id-icon="coronita:close" clear-pwd-icon="coronita:close" toggle-pwd-icon="coronita:visualize" toggle-pwd-icon-toggled="coronita:hide"></cells-credentials-form>
```

Also, using the `autofocus` attribute, the user Id field will receive focus when the page is loaded.

```html
<cells-credentials-form autofocus clear-id-icon="coronita:close" clear-pwd-icon="coronita:close" toggle-pwd-icon="coronita:visualize" toggle-pwd-icon-toggled="coronita:hide"></cells-credentials-form>
```

Tapping any of the buttons will fire an event. Activating the submit button will fire an event passing the user Id and user Password field values as payload. The submit button will be disabled until both fields are filled.

The component uses the attribute `user-name`, with the user name as value, to treat the user as already logged. This will hide the userId field and show a user greeting and a "change user" button above the password field.

```html
<cells-credentials-form user-name="John" user-id="loremipsum" clear-pwd-icon="coronita:close" toggle-pwd-icon="coronita:visualize" toggle-pwd-icon-toggled="coronita:hide"></cells-credentials-form>
```

The component can optional use cells-input-validation-behavior to validate and mask input user login value.

```html
<cells-credentials-form user-name="John" user-id="loremipsum"  input-user-options="[[inputUserOptions]]"></cells-credentials-form>
```
```js
  {
    autoValidate: true,
    inputStatusValidate: true,
    inputType: 'text',
    allowedValue: "rut",
    errorMessage: 'rutMsg',
    errorMessageIcon: 'coronita:error',
    mask: 'rut',
    maxLength: '13'
  }

```

Also, the component can show a selector for select a document type and validate the identity of the user. The selector is shown when document-type property is filled.

````html
 <cells-credentials-form id="ui"
    document-type="{{itemSelected}}"
    clear-id-icon="coronita:close"
    clear-pwd-icon="coronita:close"
    toggle-pwd-icon="coronita:visualize"
    toggle-pwd-icon-toggled="coronita:hide">
  </cells-credentials-form>
````

Document type is an object that has the rules to validate username, error message and other stuff related with the userId validation.

````json
{
    id: 'M',
    allowedValue: '^[\\w-_/.]{3,15}$',
    maxLength: 15,
    max: 15,
    min: 3,
    type: 'text',
    errorMessageIcon: 'coronita:alert',
    errorMessage: 'glomo-login-generic-error-message',
    name: 'glomo-login-document-type-M'
  }
````

The selector shown is a fake selector that fires an "select-document-type" event when is clicked. Usually the selector values are represented in into a cells-modal-selector as it can be seen in the demo.


## Icons

Since this component uses icons, it will need an [iconset](https://bbva.cellsjs.com/guides/best-practices/cells-icons.html) in your project as an [application level dependency](https://bbva.cellsjs.com/guides/advanced-guides/application-level-dependencies.html). In fact, this component uses an iconset in its demo.

## Styling

The following custom properties and mixins are available for styling:

### Custom Properties
| Custom Property                                                        | Selector                                               | CSS Property                            | Value        |
| ---------------------------------------------------------------------- | ------------------------------------------------------ | --------------------------------------- | ------------ |
| --cells-fontDefault                                                    | :host                                                  | font-family                             | sans-serif   |
| --cells-credentials-form-input-min-height                              | :host                                                  | --cells-molecule-input-field-min-height | 3.75rem      |
| --cells-credentials-form-fake-selector-bg-color                        | .credentials-form .fake-selector                       | background-color                        | --bbva-100   |
| --cells-credentials-form-fake-selector-modal-icon-color                | .credentials-form .fake-selector .modal-icon           | color                                   | --bbva-white |
| --cells-credentials-form-fake-selector-label-color                     | .credentials-form .fake-selector .label                | color                                   | --bbva-500   |
| --cells-credentials-form-fake-selector-label-selected-color            | .credentials-form .fake-selector .label.selected       | color                                   | --bbva-400   |
| --cells-credentials-form-fake-selector-placeholder-color               | .credentials-form .fake-selector .placeholder          | color                                   | --bbva-600   |
| --cells-credentials-form-fake-selector-placeholder-selected-icon-color | .credentials-form .fake-selector .placeholder.selected | color                                   | --bbva-white |
### @apply
| Mixins                                                                  | Selector                                                                                     | Value |
| ----------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- | ----- |
| --cells-credentials-form-input-field-withcontent                        | :host > --cells-molecule-input-withcontent-field-input:                                      | {}    |
| --cells-credentials-form-input-wrapper                                  | :host > --cells-molecule-input-wrapper:                                                      | {}    |
| --cells-credentials-form                                                | :host                                                                                        | {}    |
| --cells-molecule-input-has-content-wrapper                              | .credentials-form > --cells-credentials-form-cells-molecule-input-has-content-wrapper:       | {}    |
| --cells-credentials-form-form                                           | .credentials-form                                                                            | {}    |
| --cells-credentials-form-inputs                                         | .credentials-form__inputs                                                                    | {}    |
| --cells-credentials-form-inputs-input                                   | .credentials-form__inputs cells-molecule-input                                               | {}    |
| --cells-credentials-form-inputs-input-withcontent                       | .credentials-form__inputs cells-molecule-input[has-content]                                  | {}    |
| --cells-credentials-form-submit                                         | .credentials-form__submit                                                                    | {}    |
| --cells-credentials-form-submit-button                                  | .credentials-form__submit button[type="submit"]                                              | {}    |
| --cells-credentials-form-extra-actions                                  | .credentials-form__extra-actions                                                             | {}    |
| --cells-credentials-form-extra-actions-logged                           | :host([is-logged]) .credentials-form__extra-actions                                          | {}    |
| --cells-credentials-form-actions                                        | .credentials-form__forgotten-pwd                                                             | {}    |
| --cells-credentials-form-actions                                        | .credentials-form__change-user                                                               | {}    |
| --cells-credentials-form-actions                                        | .credentials-form__register                                                                  | {}    |
| --cells-credentials-form-actions-buttons                                | .credentials-form__forgotten-pwd button                                                      | {}    |
| --cells-credentials-form-actions-buttons                                | .credentials-form__change-user button                                                        | {}    |
| --cells-credentials-form-actions-buttons                                | .credentials-form__register button                                                           | {}    |
| --cells-credentials-form-forgotten-pwd                                  | .credentials-form__forgotten-pwd                                                             | {}    |
| --cells-credentials-form-change-user                                    | .credentials-form__change-user                                                               | {}    |
| --cells-credentials-form-register                                       | .credentials-form__register                                                                  | {}    |
| --cells-credentials-form-register-is-logged                             | :host([is-logged]) .credentials-form__register                                               | {}    |
| --cells-credentials-form-user                                           | .credentials-form__user                                                                      | {}    |
| --cells-credentials-form-user-image                                     | .credentials-form__user__image                                                               | {}    |
| --cells-credentials-form-user-greeting                                  | .credentials-form__user__greeting                                                            | {}    |
| --cells-credentials-form-user-name                                      | .credentials-form__user__name                                                                | {}    |
| --cells-credentials-form-fake-selector                                  | .credentials-form .fake-selector                                                             | {}    |
| --cells-credentials-form-fake-selector-focus                            | .credentials-form .fake-selector:focus                                                       | {}    |
| --cells-credentials-form-fake-selector-modal-icon                       | .credentials-form .fake-selector .modal-icon                                                 | {}    |
| --layout-vertical                                                       | .credentials-form .fake-selector .header-texts                                               | {}    |
| --cells-credentials-form-fake-selector-label                            | .credentials-form .fake-selector .label                                                      | {}    |
| --cells-credentials-form-fake-selector-placeholder                      | .credentials-form .fake-selector .placeholder                                                | {}    |
| --cells-credentials-form-animated-user                                  | :host([animated]) .credentials-form__user                                                    | {}    |
| --cells-credentials-form-animated-change-user                           | :host([animated]) .credentials-form__change-user                                             | {}    |
| --cells-credentials-form-animated-inputs-button                         | :host([animated]) .credentials-form__inputs button                                           | {}    |
| --cells-credentials-form-animated-inputs-button-molecule-input-not-last | :host([animated]) .credentials-form__inputs button ~ cells-molecule-input:not(:last-of-type) | {}    |
| --cells-credentials-form-animated-cells-molecule-input-first            | :host([animated]) .credentials-form__inputs cells-molecule-input:first-of-type               | {}    |
| --cells-credentials-form-animated-cells-molecule-input-last             | :host([animated]) .credentials-form__inputs cells-molecule-input:last-of-type                | {}    |
| --cells-credentials-form-animated-submit-button                         | :host([animated]) .credentials-form__submit button                                           | {}    |
| --cells-credentials-form-animated-extra-actions-p                       | :host([animated]) .credentials-form__extra-actions p                                         | {}    |
