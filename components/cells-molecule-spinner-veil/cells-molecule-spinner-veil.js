Polymer({
  is: 'cells-molecule-spinner-veil',

  properties: {
    /**
     * Status of spinner-veil
     */
    open: {
      type: Boolean,
      observer: '_setToggleSpinner'
    },

    _threads: {
      type: Number,
      value: 0
    }
  },

  /**
   * Open or hide spinner based on open property
   */
  _setToggleSpinner: function(open) {
    if (open) {
      this.show();
    } else {
      this.hide();
    }
  },
  /**
   * Shows spinner
   */
  show: function() {
    this.classList.add('is-visible');
    this._threads++;
  },
  /**
   * Hides spinner
   */
  hide: function() {

    if (--this._threads <= 0) {
      this.classList.remove('is-visible');

      // Since we could have _threads < 0, it is better
      // to reinitialize it to avoid negative values
      // which would lead the spinner to hide when it should not
      this._threads = 0;
    }
  }
});
