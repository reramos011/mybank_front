var btnOpen = document.querySelector('.btn-open');
var btnOpenCustom = document.querySelector('.btn-open-custom');
var btnClose = document.querySelector('.btn-close');

btnOpen.addEventListener('click', function() {
  var elem = document.querySelector('cells-molecule-spinner-veil');
  elem.set('open', true);
});

btnClose.addEventListener('click', function() {
  var elems = document.querySelectorAll('cells-molecule-spinner-veil');
  for (var i = 0; i < elems.length; i++) {
    elems[i].set('open', false);
  }
});

btnOpenCustom.addEventListener('click', function() {
  var elem = document.querySelector('.custom-style');
  elem.set('open', true);
});
