# Changelog

<!-- Document the breaking changes when updating the component as major version.
As well as the necessary changes to upgrade the component (migration guide type).
-->

## v10.0.0

### New features

- Hybrid support
- Ability to use a custom spinner provided in light DOM (`slot name="spinner"`)
- Ability to use a solid background by adding the class `solid`
- Ability to use a fade in/out effect by adding the class `fade`
