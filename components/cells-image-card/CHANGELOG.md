# CHANGELOG

## v2.0.0

### Breaking changes

- Replace `levelHeading` attribute to `headingLevel`.

- Replace `image` attribute to `imageUrl`.

- Remove `cardType`attribute.

### New things in demo component

- Removed `cells-demo-mobile`.

- Added in demo `cells-demo-helper` for demos, (copy and paste).
