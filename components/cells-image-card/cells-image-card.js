(function() {
  'use strict';
  /**
   * @customElement
   * @polymer
   * @demo demo/index.html
   */
  Polymer({

    is: 'cells-image-card',
    properties: {
      /**
       * The heading of the card.
       */
      heading: {
        type: String,
        observer: '_getHeaderHeight',
      },
      /**
       * Aria Level heading of the card
       */
      headingLevel: {
        type: Number,
        value: 1,
      },
      /**
       * The subheading of the card.
       */
      subheading: {
        type: String,
        value: null,
      },
      /**
       * The tags of the card.
       */
      tags: {
        type: String,
        value: null,
      },
      /**
       * The tag render bottom of heading
       */
      tagsBottomHeading: {
        type: Boolean,
        value: false,
      },
      /**
       * The heading and tags render inside image.
       */
      headerInsideImage: {
        type: Boolean,
        value: false,
      },
      /**
       * The url of the title image of the card.
       */
      imageUrl: {
        type: String,
        value: null,
      },
      /**
       * The text alternative of the card's image.
       */
      alt: {
        type: String,
        value: '',
      },
      /**
       * The link of the card
       */
      linkUrl: {
        type: String,
      },
      /**
       * The text of the link at the bottom of the card
       */
      linkText: {
        type: String,
        value: null,
      },
      /**
       * The card is type product
       */
      productCard: {
        type: Boolean,
        value: false,
      },
    },
    /**
     * Returns the class for the heading depending on the value of 'headerInsideImage' property
     * @param {Boolean} headerInsideImage
     * @return {String}
     */
    _getHeadingClass(headerInsideImage) {
      return (!headerInsideImage) ? 'header' : 'header-inside';
    },
    /**
     * Sets the position of the heading inside the image
     */
    _getHeaderHeight() {
      if (this.headerInsideImage) {
        let element = this.$.headerCard;
        this.async(() => {
          let headerCardHeight = element.clientHeight;
          this.updateStyles({
            '--cells-image-cared-wrapper-card-header-inside-margin-top': '-' + headerCardHeight + 'px',
          });
        });
      }
    },
    /**
     * Returns the class for the position of the tags depending on the value of 'tagsBottomHeading' property
     * @param {Boolean} tagsBottomHeading
     * @return {String}
     */
    _getTagPosition(tagsBottomHeading) {
      return (tagsBottomHeading) ? 'heading-tag-bottom' : '';
    },
    /**
     * Returns the class for the card depending on the value of 'productCard' property
     * @param {Boolean} productCard
     * @return {String}
     */
    _getCardClass(productCard) {
      return (productCard) ? 'card-product' : '';
    },
  });
}());
