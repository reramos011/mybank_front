(function(document) {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'login': '/',
      'accounts': '/accounts',
      'register': '/register',
      'movements': '/movements',
      'operations': '/operations',
    }
  });

}(document));
