# Imagen raiz
FROM reramos011/cells

# Set env variables
ARG BBVA_USER
ARG API_KEY

# Carpeta raiz
WORKDIR /myBank

# Copia de archivos de carpeta local
ADD . /myBank

# Para evitar problemas de permisos se usa el usuario root
USER root

# Se instalan las dependencias del proyecto
RUN npm install

RUN touch ~/.bowerrc && \
    echo '{ \n\
     "timeout": 120000, \n\
     "registry": "https://'"$BBVA_USER"':'"$API_KEY"'@globaldevtools.bbva.com/artifactory/api/bower/bower-repo", \n\
     "resolvers": [ \n\
         "bower-art-resolver" \n\
     ] \n\
 }' > ~/.bowerrc

# Se instalan los componentes
RUN bower install --allow-root

# Puerto en el que se expone la app
EXPOSE 8001

RUN chown -R 1001 /myBank && \
chgrp -R 0 /myBank && \
chmod -R 777 /myBank

USER 1001

# Comando de inicializacion
CMD ["cells", "app:serve", "-c", "prod.json"]
